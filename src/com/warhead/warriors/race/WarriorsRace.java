package com.warhead.warriors.race;

import java.util.ArrayList;

/**
 *
 * @author somners
 */
public class WarriorsRace {
    
    public String name;
    public Health health = null;
    public Mana mana = null;
    public Stamina stamina = null;
    public ArrayList<String> bannedClasses = new ArrayList<String>();
    
}
