/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.warhead.warriors.api.Util;

/**
 *
 * @author Somners
 */
public enum SkillPointType {
    
    MANA("mana"),
    STAMINA("stamina"),
    HEALTH("health"),
    MANA_REGEN("mana regen"),
    STAMINA_REGEN("stamina regen"),
    HEALTH_REGEN("health regen"),
    ITEM_DAMAGE("item damage");

    private String name;

    SkillPointType(String name){
        this.name = name;
    }

    public String getName(){
        return name;
    }
    
    public static boolean exists(String name){
        boolean exists = false;
        for(SkillPointType type :SkillPointType.values()){
            if(type.getName().equalsIgnoreCase(name)){
                exists = true;
                break;
            }
        }
        return exists;
    }
    
    public boolean equalsName(String name){
        return this.name.equalsIgnoreCase(name);
    }
    
}
