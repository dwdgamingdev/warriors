package com.warhead.warriors.api.races;

/**
 *
 * @author somners
 */
public interface Race {

    /**
     * get the name of this Race
     * @return the name
     */
    public String getName();

    public int getBaseMana();

    public int getMaxMana();

    public int getBaseStamina();

    public int getMaxStamina();

    public int getBaseHealth();

    public int getMaxHealth();

    public int getSPHealthModifier();

    public int getSPManaModifier();

    public int getSPStaminaModifier();
    
    public int getManaRegenInterval();
    
    public float getManaRegenRate();
    
    public int getManaSPRegenModifier();
    
    public int getStaminaRegenInterval();
    
    public float getStaminaRegenRate();
    
    public int getStaminaSPRegenModifier();
    
    public int getHealthRegenInterval();
    
    public float getHealthRegenRate();
    
    public int getHealthSPRegenModifier();

}
