package com.warhead.warriors.api.warcharacter;

import com.warhead.warriors.api.achievement.Achievement;
import java.util.List;

/**
 *
 * @author somners
 */
public interface WarCharacter {

    /**
     * get the name of this WarCharacter
     * @return the name
     */
    public String getName();

    /**
     * check if a player can use this skill<br>
     * player must belong to this character class for it to work
     * @param player player you are checking for
     * @param skillName name of the skill you are checking
     * @return true = they can use the skill<br>
     * false = they cannot use the skill
     */
    public boolean canCharacterUseSkill(String skillName);

    public int getToolBaseDamage(String tool);
    
    public int getToolMaxDamage(String tool);
    
    public int getToolDamageModifier(String tool);

    public boolean canUseRace(String raceName);
    
    public WarCharacterFile getWarCharacterFile();
    
    public int getSkillManaCost(String skill);
    
    public int getSkillStaminaCost(String skill);
    
    public boolean canCharacterUseTool(String toolName);
    
    public List<String> getSkillNames();
    
    public List<Achievement> getSkillAchievements(String skill);
    
    public int getSkillLevel(String skill);
    
    public int getSkillCooldown(String skill);
    
    public List<Achievement> getToolAchievements(String tool);
    
    public int getToolLevel(String tool);

    public List<String> getBannedRaces();
    
    public List<String> getToolNames();
    
}
