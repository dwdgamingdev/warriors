/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.warhead.warriors.api;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.spout.api.Spout;
import org.spout.api.entity.Player;
import org.spout.vanilla.component.entity.inventory.PlayerInventory;

/**
 *
 * @author Somners
 */
public class SkillBindManager {
    
    private HashMap<String, HashMap<String, List<String>>> bindMap;
    private static SkillBindManager instance = null;
    
    public SkillBindManager(){
        bindMap = new HashMap<String, HashMap<String, List<String>>>();
        instance = this;
    }
    
    public static SkillBindManager get(){
        return instance;
    }
    
    public boolean bindSkill(Player player, String skillName){
        String playerName = player.getName();
        String itemName = this.getItemName(player);
        if(!bindMap.containsKey(playerName)){
            bindMap.put(playerName, new HashMap<String, List<String>>());
        }
        if(!bindMap.get(playerName).containsKey(itemName)){
            bindMap.get(playerName).put(itemName, new ArrayList<String>());
        }
        if(bindMap.get(playerName).get(itemName).contains(skillName)){
            return false;
        }
        bindMap.get(playerName).get(itemName).add(skillName);
        for(int i = 0; i < bindMap.size(); i++){
            Spout.getLogger().info(bindMap.keySet().toArray()[i].toString());
        }
        return true;
    }
    
    public boolean unBindSkill(Player player, String skillName){
        String playerName = player.getName();
        String itemName = this.getItemName(player);
        if(bindMap.containsKey(playerName)){
            if(Spout.debugMode())
                Spout.getLogger().info("bindMap has " + playerName);
            if(bindMap.get(playerName).containsKey(itemName)){
                if(Spout.debugMode())
                    Spout.getLogger().info(playerName + " contains " + itemName);
                if(bindMap.get(playerName).get(itemName).contains(skillName)){
                    if(Spout.debugMode())
                        Spout.getLogger().info(itemName + " has " + skillName + " Bound");
                    bindMap.get(playerName).get(itemName).remove(skillName);
                    return true;
                } else {
                    if(Spout.debugMode())
                        Spout.getLogger().info(itemName + "does NOT have " + skillName + " Bound");
                    return false;
                }
            } else {
                if(Spout.debugMode())
                    Spout.getLogger().info(playerName + " does NOT contain " + itemName);
                return false;
            }
        } else {
            if(Spout.debugMode())
                Spout.getLogger().info("bindMap does NOT have " + playerName);
            return false;
        }
    }
    
    public boolean isBound(Player player, String skillName){
        String playerName = player.getName();
        String itemName = this.getItemName(player);
        if(bindMap.containsKey(playerName)){
            if(Spout.debugMode())
                Spout.getLogger().info("bindMap has " + playerName);
            if(bindMap.get(playerName).containsKey(itemName)){
                    if(Spout.debugMode())
                        Spout.getLogger().info(itemName + " has " + skillName + " Bound");
                    return bindMap.get(playerName).get(itemName).contains(skillName);
            } else {
                if(Spout.debugMode())
                    Spout.getLogger().info(playerName + " does NOT contain " + itemName);
                return false;
            }
        } else {
            if(Spout.debugMode())
                Spout.getLogger().info("bindMap does NOT have " + playerName);
            return false;
        }
    }
    
    public String[] getBoundSkillNames(String playerName, String itemName){
        if(bindMap.containsKey(playerName)){
            if(bindMap.get(playerName).containsKey(itemName)){
                    return (String[]) bindMap.get(playerName).get(itemName).toArray();
            }
        }
        return null;
    }
    
    private String getItemName(Player player){
        String toolName;
        try{
            toolName = player.get(PlayerInventory.class).getQuickbar().getSelectedSlot().get().getMaterial().getDisplayName();
        }
        catch(NullPointerException npe){
            toolName = "Fist";
        }
        return toolName;
    }
}
