
package com.warhead.warriors.api.components;

import com.warhead.warheadutils.database.MySQLManager;
import com.warhead.warriors.Warriors;
import org.spout.api.component.type.EntityComponent;
import org.spout.api.entity.Player;

/**
 *
 * @author somners
 */
public class SkillPointComponent extends EntityComponent implements WarComponent{

    private int toolSP;
    private int healthSP;
    private int manaSP;
    private int staminaSP;
    private int unusedSP;
    private int toolDamageSP;
    private MySQLManager mysql;
    private boolean isLoaded = false;

    @Override
    public void onAttached(){
        super.onAttached();
        mysql = Warriors.getDatabase();
        isLoaded = true;
    }



    @Override
    public void readFromDatabase(){
        unusedSP = mysql.getIntValue("SELECT `unused` FROM `warriors_skill_points` WHERE `player_name` = '"+ ((Player)super.getOwner()).getName() +"'", "unused");
    }


    @Override
    public void writeToDatabase(){
        if(isLoaded){
            mysql.update("UPDATE `warriors_skill_points` SET `unused` = '"+ unusedSP +"' WHERE `player_name` = '"+ ((Player)super.getOwner()).getName() +"' LIMIT 1");
        }
    }



    public int getSkillPoints() {
        return unusedSP;
    }


    public void addSkillPoint() {
        unusedSP++;
    }


    public void removeSkillPoint() {
        unusedSP--;
    }
    
    public void setSkillPoint(int toSet) {
        unusedSP = toSet;
    }
    
    public void addSkillPoints(int toAdd){
        unusedSP =unusedSP + toAdd;
    }
    
    public void removeSkillPoints(int toRemove){
        unusedSP = unusedSP - toRemove < 0 ? 0 : unusedSP - toRemove;
    }

    @Override
    public void refreshComponent() {
        writeToDatabase();
        isLoaded = true;
        readFromDatabase();
    }
    
}