package com.warhead.warriors.api.components;

import com.warhead.warheadutils.database.MySQLManager;
import com.warhead.warriors.Warriors;
import org.spout.api.component.type.EntityComponent;
import org.spout.api.entity.Player;

/**
 *
 * @author somners
 */
public class XpComponent extends EntityComponent implements WarComponent{

    private int experience;
    private MySQLManager mysql;
    private boolean isLoaded = false;

    @Override
    public void onAttached(){
        super.onAttached();
        mysql = Warriors.getDatabase();
    }

    @Override
    public void onDetached() {

    }

    @Override
    public void refreshComponent(){
        if(isLoaded){
            writeToDatabase();
        }
        isLoaded = true;
        readFromDatabase();
    }

    @Override
    public void writeToDatabase() {
        mysql.update("UPDATE `warriors_players` SET `xp` = '"+ experience +"' WHERE `player_name` = '"+ ((Player)super.getOwner()).getName() +"' LIMIT 1");
    }

    @Override
    public void readFromDatabase() {
        experience = mysql.getIntValue("SELECT `xp` FROM `warriors_players` WHERE `player_name` = '"+ ((Player)super.getOwner()).getName() +"'", "xp");
    }

    public void addXp(int toAdd) {
        experience += toAdd;
    }


    public void removeXp(int toRemove) {
        experience -= toRemove;
    }

    public int getXp() {
        return experience;
    }


    public int getLevel() {
        return 0;
    }
}
