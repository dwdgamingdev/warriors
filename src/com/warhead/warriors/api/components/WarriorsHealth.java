package com.warhead.warriors.api.components;

import com.warhead.warheadutils.database.MySQLManager;
import com.warhead.warriors.Warriors;
import org.spout.api.entity.Player;
import org.spout.vanilla.component.entity.misc.Health;
import org.spout.vanilla.event.cause.DamageCause;
import org.spout.vanilla.event.cause.HealthChangeCause;

/**
 *
 * @author somners
 */
public class WarriorsHealth implements WarComponent{

    private Player player;
    private MySQLManager mysql;
    private int healthSP;
    private int healthSPMod;
    private int healthBase;
    private int healthMax;

    public WarriorsHealth(Player player) {
        this.player = player;
        mysql = Warriors.getDatabase();
        readFromDatabase();
        healthSPMod = player.get(RPGComponent.class).getRace().getSPHealthModifier();
        healthMax = player.get(RPGComponent.class).getRace().getMaxHealth();
        healthBase = player.get(RPGComponent.class).getRace().getBaseHealth();
        setMaxHealth(healthBase + (healthSP*healthSPMod));
    }
    
    @Override
    public void readFromDatabase() {
        healthSP = mysql.getIntValue("SELECT `health` FROM `warriors_skill_points` WHERE `player_name` = '"+ player.getName() +"'", "health");
        
    }

    @Override
    public void writeToDatabase() {
        mysql.update("UPDATE `warriors_skill_points` SET `health` = '"+ healthSP +"' WHERE `player_name` = '"+ player.getName() +"' LIMIT 1");
    }

    @Override
    public void refreshComponent() {
        writeToDatabase();
        healthSPMod = player.get(RPGComponent.class).getRace().getSPHealthModifier();
        healthMax = player.get(RPGComponent.class).getRace().getMaxHealth();
        healthBase = player.get(RPGComponent.class).getRace().getBaseHealth();
        setMaxHealth(healthBase + (healthSP*healthSPMod));
        
    }

    public int getHealth() {
        return player.get(Health.class).getHealth();
    }


    public int getMaxhHealth() {
        return player.get(Health.class).getMaxHealth();
    }


    public void setHealth(int toSet) {
        player.get(Health.class).setHealth(toSet, HealthChangeCause.UNKNOWN);
    }


    public void setMaxHealth(int toSet) {
        player.get(Health.class).setMaxHealth(toSet);
    }

    public void addHealth(int toAdd) {
        Health hComp = player.get(Health.class);
        hComp.setHealth(hComp.getHealth() + toAdd, HealthChangeCause.UNKNOWN);
    }

    public void removeHealth(int toRemove, DamageCause cause) {
        player.get(Health.class).damage(toRemove, cause, true);
    }
    
    public int getHealthPoints() {
        return healthSP;
    }


    public void addHealthPoint() {
        healthSP++;
    }


    public void removeHealthPoint() {
        healthSP--;
    }


    public void setHealthPoint(int toSet) {
        healthSP = toSet;
    }

    public boolean isHealthMaxed(){
        int diff = healthMax - healthBase;
        int mod = healthSP * healthSPMod;
        return mod >= diff;
    }

}
