package com.warhead.warriors.api.components;

import com.warhead.warheadutils.database.MySQLManager;
import com.warhead.warriors.Warriors;
import com.warhead.warriors.api.warcharacter.WarCharacter;
import java.util.List;
import org.spout.api.component.type.EntityComponent;
import org.spout.api.entity.Player;

/**
 *
 * @author somners
 */
public class ToolComponent extends EntityComponent implements WarComponent{
    
    private int damageSP = 0;
    private MySQLManager mysql;
    private boolean loaded = false;
    
    @Override
    public void onAttached(){
        super.onAttached();
        mysql = Warriors.getDatabase();
    }

    @Override
    public void onDetached(){

    }

    @Override
    public void refreshComponent(){
        if(!loaded){
            readFromDatabase();
        }
        else{
            writeToDatabase();
        }
    }

    @Override
    public void readFromDatabase() {
        damageSP = mysql.getIntValue("SELECT `tool_damage` FROM `warriors_skill_points` WHERE `player_name` = '"+ ((Player)super.getOwner()).getName() +"'", "tool_damage");
    }

    @Override
    public void writeToDatabase() {
        mysql.update("UPDATE `warriors_skill_points` SET `tool_damage` = '"+ damageSP +"' WHERE `player_name` = '"+ ((Player)super.getOwner()).getName() +"' LIMIT 1");
    }
    
    public int getDamageModifier(String itemName){
        RPGComponent rpg = ((Player)super.getOwner()).get(RPGComponent.class);
        if(rpg.hasSecondaryClass() && this.canSecondaryClassUseItem(itemName)){
            return rpg.getSecondaryClass().getToolDamageModifier(itemName);
        }
        if(this.canPrimaryClassUseItem(itemName)){
            return rpg.getPrimaryClass().getToolDamageModifier(itemName);
        }
        return -1;
    }
    
    public int getMaxDamage(String itemName){
        RPGComponent rpg = ((Player)super.getOwner()).get(RPGComponent.class);
        if(rpg.hasSecondaryClass() && this.canSecondaryClassUseItem(itemName)){
            return rpg.getSecondaryClass().getToolMaxDamage(itemName);
        }
        if(this.canPrimaryClassUseItem(itemName)){
            return rpg.getPrimaryClass().getToolMaxDamage(itemName);
        }
        return -1;
    }
    
    public int getBaseDamage(String itemName){
        RPGComponent rpg = ((Player)super.getOwner()).get(RPGComponent.class);
        if(rpg.hasSecondaryClass() && this.canSecondaryClassUseItem(itemName)){
            return rpg.getSecondaryClass().getToolBaseDamage(itemName);
        }
        if(this.canPrimaryClassUseItem(itemName)){
            return rpg.getPrimaryClass().getToolBaseDamage(itemName);
        }
        return -1;
    }
    
    public int getDamageSP(){
        return damageSP;
    }
    
    public void addDamageSP(int toAdd){
        damageSP = damageSP + toAdd;
    }
    
    public void removeDamageSP(int toRemove){
        damageSP = damageSP - toRemove < 0 ? 0 : damageSP - toRemove;
    }
    
    public void setDamageSP(int toSet){
        damageSP = toSet;
    }
    
    public boolean canPrimaryClassUseItem(String itemName){
        if(!((Player)super.getOwner()).get(RPGComponent.class).getPrimaryClass().canCharacterUseTool(itemName)){
            return false;
        }
        return ((Player)super.getOwner()).get(RPGComponent.class).canPrimaryUseItem(itemName);
    }
    
    public boolean canSecondaryClassUseItem(String itemName){
        if(!((Player)super.getOwner()).get(RPGComponent.class).getPrimaryClass().canCharacterUseTool(itemName)){
            return false;
        }
        return ((Player)super.getOwner()).get(RPGComponent.class).canSecondaryUseItem(itemName);
    }
    
    public int getDamage(String itemName){
        int max = this.getBaseDamage(itemName);
        int min = this.getMaxDamage(itemName);
        int mod = this.getDamageModifier(itemName);
        return min + (mod * this.damageSP) > max ? max : min + (mod * this.damageSP);
    }
    
    public boolean isDamageSPMaxed(){
        WarCharacter character = ((Player)super.getOwner()).get(RPGComponent.class).getPrimaryClass();
        WarCharacter scharacter = ((Player)super.getOwner()).get(RPGComponent.class).getSecondaryClass();
        List<String> tools = character.getToolNames();
        for(String wName : tools){
            if(scharacter != null && this.canSecondaryClassUseItem(wName)){
                int mod = scharacter.getToolDamageModifier(wName);
                int max = scharacter.getToolBaseDamage(wName);
                int min = scharacter.getToolMaxDamage(wName);
                if(max > min + (mod*damageSP)){
                    return false;
                }
            }
            else if(this.canPrimaryClassUseItem(wName)){
                int mod = character.getToolDamageModifier(wName);
                int max = character.getToolBaseDamage(wName);
                int min = character.getToolMaxDamage(wName);
                if(max > min + (mod*damageSP)){
                    return false;
                }
            }
        }
        return true;
    }
}
