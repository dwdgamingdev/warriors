package com.warhead.warriors.api.skill;

import java.util.HashMap;

/**
 *
 * @author somners
 */
public interface SkillDescriptionFile {

    public String getName();

    public String getMainClass();

    public HashMap<String, Object> getWarCharacterConfigEntry();

}
