
package com.warhead.warriors.api.skill.command;

import com.warhead.warriors.api.skill.Skill;
import org.spout.api.command.CommandContext;
import org.spout.api.entity.Player;

/**
 *
 * @author Somners
 */
public interface SkillCommand {
    
    public void execute(CommandContext cc, Player player);
    
    /**
     * Should return a string array like this:<br>
     * new String[] {"/usage", "description"}
     * @return 
     */
    public String[] getHelpMessage();
    
    public Skill getSkill();
}
