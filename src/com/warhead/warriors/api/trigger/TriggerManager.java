package com.warhead.warriors.api.trigger;

import com.warhead.warriors.Warriors;
import com.warhead.warriors.api.WarriorsManager;
import com.warhead.warriors.api.components.RPGComponent;
import com.warhead.warriors.api.skill.Skill;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Level;
import org.spout.api.Spout;

/**
 *
 * @author somners
 */
public class TriggerManager {

    private static HashMap<Skill, ArrayList<TriggerListener>> listenerMap = new HashMap<Skill, ArrayList<TriggerListener>>();

    public TriggerManager(){

    }

    public static void registerListener(TriggerListener listener, Skill skill){
        if(!listenerMap.containsKey(skill)){
            ArrayList list = new ArrayList<TriggerListener>();
            list.add(listener);
            listenerMap.put(skill, list);
            return;
        }
        listenerMap.get(skill).add(listener);
    }

    public static void unregisterListener(Skill skill){
        if(listenerMap.containsKey(skill)){
            listenerMap.remove(skill);
        }
    }

    
    // @TODO optimize the calling of triggers.
    public static void processListenersForExecute(Trigger trigger){
        for(Skill skillSet: listenerMap.keySet()){
            RPGComponent rpg = trigger.getPlayer().get(RPGComponent.class);
            if(rpg.canPrimaryUseSkill(skillSet.getname()) || rpg.canSecondaryUseSkill(skillSet.getname())){
                if(Spout.debugMode())
                    Warriors.getLog().info(" can use skill check passed.");
                if(rpg.hasSecondaryClass() && rpg.canSecondaryUseSkill(skillSet.getname())){
                    if((rpg.getSecondaryClass().getSkillCooldown(skillSet.getname()) < 0)){
                        if(Spout.debugMode())Warriors.getLog().severe(" Error calling listener for "+skillSet.getname()+" for player "+rpg.getPlayerName());
                        continue;
                    }
                    if(rpg.getSecondaryClass().getSkillCooldown(skillSet.getname()) > 0 && WarriorsManager.getCooldownManager().isInCooldown(rpg.getPlayerName(), skillSet.getname())){
                        if(Spout.debugMode())Warriors.getLog().severe(" cooldown check failed");
                        continue;
                    }
                }
                else{
                    if((rpg.getPrimaryClass().getSkillCooldown(skillSet.getname()) < 0)){
                        Warriors.getLog().severe(" Error calling listener for "+skillSet.getname()+" for player "+rpg.getPlayerName());
                        continue;
                    }
                    if(rpg.getPrimaryClass().getSkillCooldown(skillSet.getname()) > 0 && WarriorsManager.getCooldownManager().isInCooldown(rpg.getPlayerName(), skillSet.getname())){
                        Warriors.getLog().severe(" cooldown check failed");
                        continue;
                    }
                }
                for(TriggerListener listener : listenerMap.get(skillSet)){
                    if(execute(trigger, listener)){
                        WarriorsManager.getCooldownManager().addCooldown(rpg.getPlayerName(), skillSet.getname(), rpg.getPrimaryClass().getSkillCooldown(skillSet.getname()));
                    }
                    if(Spout.debugMode())
                        Warriors.getLog().info(" listener executed: " + skillSet.getname() + "  Player: " + trigger.getPlayer().getName());
                }
            }
        }
    }

    public static boolean execute(Trigger trigger, TriggerListener executor){
        Method[] method = executor.getClass().getMethods();
        for(int i = 0; i < method.length ; i++){
            if(method[i].isAnnotationPresent(TriggerHandler.class) && method[i].getParameterTypes()[0].equals(trigger.getClass()) && method[i].getReturnType().equals(boolean.class)){
                try {
                    return method[i].invoke(executor, trigger).equals(true);
                } catch (IllegalAccessException ex1) {
                    Warriors.getLog().severe("Unable to execute TriggerListener in: "+executor.getClass().getName());
                    ex1.printStackTrace();
                } catch (IllegalArgumentException ex1) {
                   Warriors.getLog().severe("Unable to execute TriggerListener in: "+executor.getClass().getName());
                    ex1.printStackTrace();
                } catch (InvocationTargetException ex1) {
                    Warriors.getLog().severe("Unable to execute TriggerListener in: "+executor.getClass().getName());
                    ex1.printStackTrace();
                }
            }
        }
        return false;
    }
}
