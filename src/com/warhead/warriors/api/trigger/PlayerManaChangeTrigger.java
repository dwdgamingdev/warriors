package com.warhead.warriors.api.trigger;

import com.warhead.warriors.api.components.ManaChangeCause;
import com.warhead.warriors.api.components.ManaComponent;
import org.spout.api.entity.Player;

/**
 *
 * @author somners
 */
public class PlayerManaChangeTrigger extends Trigger{

    private Player player;
    private int oldMana;
    private int newMana;
    private ManaChangeCause source;
    private boolean isCanceled = false;
    private static final TriggerType type = TriggerType.PlayerManaChange;


    public PlayerManaChangeTrigger(Player player, int oldMana, int newMana, ManaChangeCause source){
        this.player = player;
        this.oldMana = oldMana;
        this.newMana = newMana;
        this.source = source;
    }

    @Override
    public void setTriggerEventCanceled(boolean isCanceled) {
        this.isCanceled = isCanceled;
    }

    @Override
    public void cancelTriggerEvent() {
        isCanceled = true;
    }

    @Override
    public boolean isTriggerEventCanceled() {
        return isCanceled;
    }

    @Override
    public TriggerType getTriggerType() {
        return type;
    }

    public int getOldMana(){
        return oldMana;
    }

    public int getNewMana(){
        return newMana;
    }

    public int getManaChange(){
        return Math.abs(oldMana - newMana);
    }

    public ManaChangeCause getSource(){
        return source;
    }

    @Override
    public Player getPlayer(){
        return player;
    }

    public ManaComponent getManaComponent(){
        return player.get(ManaComponent.class);
    }

}
