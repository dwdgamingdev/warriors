
package com.warhead.warriors.character;

import com.warhead.warriors.api.achievement.Achievement;
import com.warhead.warriors.api.warcharacter.WarCharacter;
import com.warhead.warriors.api.warcharacter.WarCharacterFile;
import java.util.List;
import org.spout.api.exception.ConfigurationException;

/**
 *
 * @author somners
 */
public class YamlCharacter implements WarCharacter {



    public YamlCharacter(WarCharacterFile config) throws ConfigurationException{
        
    }

    @Override
    public String getName() {
        //TODO: FINISH
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public boolean canCharacterUseSkill(String skillName) {
        //TODO: FINISH
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public int getToolBaseDamage(String tool) {
        //TODO: FINISH
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public int getToolMaxDamage(String tool) {
        //TODO: FINISH
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public int getToolDamageModifier(String tool) {
        //TODO: FINISH
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public boolean canUseRace(String raceName) {
        //TODO: FINISH
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public WarCharacterFile getWarCharacterFile() {
        //TODO: FINISH
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public int getSkillManaCost(String skill) {
        //TODO: FINISH
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public int getSkillStaminaCost(String skill) {
        //TODO: FINISH
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public boolean canCharacterUseTool(String toolName) {
        //TODO: FINISH
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public List<String> getSkillNames() {
        //TODO: FINISH
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public List<Achievement> getSkillAchievements(String skill) {
        //TODO: FINISH
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public List<Achievement> getToolAchievements(String tool) {
        //TODO: FINISH
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public List<String> getBannedRaces() {
        //TODO: FINISH
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public List<String> getToolNames() {
        //TODO: FINISH
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public int getSkillLevel(String skill) {
        //TODO: FINISH
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public int getToolLevel(String tool) {
        //TODO: FINISH
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public int getSkillCooldown(String skill) {
        //TODO: FINISH
        throw new UnsupportedOperationException("Not supported yet.");
    }

}
