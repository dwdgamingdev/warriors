package com.warhead.warriors.character;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author somners
 */
public class Skill {
    String name;
    Integer cooldown;
    Integer level;
    Integer manaCost;
    Integer staminaCost;
    List<String> achievements = new ArrayList<String>();
}
