package com.warhead.warriors.character;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author somners
 */
public class Item {
    String name;
    Integer level;
    Integer maxDamage;
    Integer baseDamage;
    Integer skillPointDamageModifier;
    List<String> achievements = new ArrayList<String>();
}
