package com.warhead.warriors.character;

import java.util.ArrayList;

/**
 *
 * @author somners
 */
public class WarriorsClass {
    String className;
    ArrayList<Item> items = new ArrayList<Item>();
    ArrayList<Skill> skills = new ArrayList<Skill>();
    ArrayList<String> bannedRaces = new ArrayList<String>();
}
