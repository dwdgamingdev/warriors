package com.warhead.warriors.listeners;

import com.warhead.warriors.api.components.RPGComponent;
import com.warhead.warriors.api.trigger.PlayerLeftClickTrigger;
import com.warhead.warriors.api.trigger.PlayerRightClickTrigger;
import com.warhead.warriors.api.trigger.TriggerManager;
import java.util.logging.Level;
import org.spout.api.Spout;
import org.spout.api.event.EventHandler;
import org.spout.api.event.Listener;
import org.spout.api.event.player.PlayerInteractEvent;

/**
 *
 * @author somners
 */
public class WarriorsTriggerListener implements Listener{
    
    
    @EventHandler
    public void onPlayerClick(PlayerInteractEvent event){
//        Warriors.getLog().info("|||TEST|||   click");
        if(event.getPlayer().get(RPGComponent.class) == null){
            return;
        }
        /*
         * Check if the player left clicked
         */
        if(event.getAction().equals(PlayerInteractEvent.Action.LEFT_CLICK)){
            PlayerLeftClickTrigger plct = new PlayerLeftClickTrigger(event);
            TriggerManager.processListenersForExecute(plct);
        }

        /*
         * Check if the player right clicked
         */
        if(event.getAction().equals(PlayerInteractEvent.Action.RIGHT_CLICK)){
            PlayerRightClickTrigger prct = new PlayerRightClickTrigger(event);
            TriggerManager.processListenersForExecute(prct);
        }
    }
    
}
