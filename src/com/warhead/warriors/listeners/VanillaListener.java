package com.warhead.warriors.listeners;

import com.warhead.warriors.Warriors;
import com.warhead.warriors.api.WarriorsManager;
import com.warhead.warriors.api.components.RPGComponent;
import com.warhead.warriors.api.components.ToolComponent;
import com.warhead.warriors.api.trigger.PlayerAttackTrigger;
import com.warhead.warriors.api.trigger.PlayerDefendTrigger;
import com.warhead.warriors.api.trigger.TriggerManager;
import org.spout.api.entity.Entity;
import org.spout.api.entity.Player;
import org.spout.api.event.EventHandler;
import org.spout.api.event.Listener;
import org.spout.api.inventory.ItemStack;
import org.spout.vanilla.component.entity.inventory.PlayerInventory;
import org.spout.vanilla.event.entity.EntityDamageEvent;
import org.spout.vanilla.event.entity.EntityEquipmentEvent;

/**
 *
 * @author somners
 */
public class VanillaListener implements Listener{

    @EventHandler
    public void onPlayerAttack(EntityDamageEvent event){
        Entity defender = event.getEntity();
        Object attacker = event.getDamageCause().getSource();
        /*
         * Check if the defender is player
         */
        if(defender instanceof Player ){
            Player player = (Player) defender;
            if(player.get(RPGComponent.class) != null){
                PlayerDefendTrigger pdt = new PlayerDefendTrigger(player, (Entity)attacker, event.getDamage(), event.getDamageCause());
                TriggerManager.processListenersForExecute(pdt);
                event.setCancelled(pdt.isTriggerEventCanceled());
                event.setDamage(pdt.getDamage());
            }
            
        }
        /*
         * Check if the attacker is a player
         */
        if(attacker instanceof Player){
            Player player = (Player) attacker;
            if (player.get(RPGComponent.class) == null) {
                return;
            }
            String itemName;
            try {

                itemName = player.get(PlayerInventory.class).getQuickbar().getSelectedSlot().get().getMaterial().getDisplayName();
            } catch (NullPointerException npe) {
                itemName = "Fist";
            }

            if(WarriorsManager.getToolManager().isManagedItem(itemName)){
                ToolComponent tComp = player.get(ToolComponent.class);
                if((!tComp.canPrimaryClassUseItem(itemName) && !tComp.canSecondaryClassUseItem(itemName)) || itemName.equals("Fist")){
                    player.sendMessage(Warriors.getChatTag(), " You cannot use: " + itemName);
                    event.setCancelled(true);
                    return;
                }
                else{
                    if(!itemName.equals("Fist")){
                        event.setDamage(tComp.getDamage(itemName));
                    }
                }
            }
            PlayerAttackTrigger pat = new PlayerAttackTrigger(player, defender, event.getDamage());
            TriggerManager.processListenersForExecute(pat);
            event.setCancelled(pat.isTriggerEventCanceled());
            event.setDamage(pat.getDefenderDamage());
        }

    }

    @EventHandler
    public void onPlayerEquip(EntityEquipmentEvent event){
//        Warriors.getLog().info("|||TEST|||   equip");
        Entity entity = event.getEntity();
        if(entity instanceof Player){
            if(((Player)entity).get(RPGComponent.class) == null){
                return;
            }
            ItemStack item = event.getItem();
            String itemName = item.getMaterial().getDisplayName();
            if(WarriorsManager.getToolManager().isManagedItem(itemName)){
                Player player = (Player)entity;
                ToolComponent tComp = player.get(ToolComponent.class);
                if((!tComp.canPrimaryClassUseItem(itemName) && !tComp.canSecondaryClassUseItem(itemName))){
                    PlayerInventory inv = player.get(PlayerInventory.class);
                    if(event.getSlot() == 0){
                        inv.getQuickbar().remove(item);
                    }
                    else{
                        int slot = event.getSlot() -1 + 100;
                        inv.getMain().remove(slot);
                        inv.getMain().add(9, 35, item);
                    }
                    player.sendMessage(Warriors.getChatTag(), " You cannot equip: " + itemName);
                }
            }
        }
    }


}