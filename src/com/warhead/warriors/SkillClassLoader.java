/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.warhead.warriors;

import com.warhead.warriors.skills.WSkillDescriptionFile;
import com.warhead.warriors.api.skill.Skill;
import com.warhead.warriors.api.skill.SkillDescriptionFile;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLClassLoader;
import java.net.URLConnection;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.spout.api.Spout;
import org.spout.api.util.config.yaml.YamlConfiguration;

/**
 *
 * @author Somners
 */
public class SkillClassLoader extends URLClassLoader{
    
    private SkillDescriptionFile skillDesc;
    
    public SkillClassLoader(File file, URL[] urls, ClassLoader parent){
        super(urls, parent);
        try {
            URL skillDescURL = super.getResource("Skill.yml");
            URLConnection skillDescConn = skillDescURL.openConnection();
            InputStream in = skillDescConn.getInputStream();
            YamlConfiguration config = new YamlConfiguration(in);
            skillDesc = new WSkillDescriptionFile(config);
        } catch (IOException ex) {
            Warriors.getLog().severe(" Read error loading skill:");
            Warriors.getLog().severe(ex.getMessage());
            Warriors.getLog().severe(" Check to see if there is a 'Skill.yml'!");
        }
    }
    
    @Override
    public void addURL(URL url){
        super.addURL(url);
    }
    
    public Skill loadSkillClass(String classPath){
        Skill skill = null;
        try {
            Class<?> skillClass = super.loadClass(classPath);
            return (Skill) skillClass.newInstance();
        } catch (InstantiationException ex) {
            Warriors.getLog().severe(" Problem Instantiating skill during load:");
            Warriors.getLog().severe(ex.getMessage());
        } catch (IllegalAccessException ex) {
            Warriors.getLog().severe(" Skill file not accessible during load:");
            Warriors.getLog().severe(ex.getMessage());
            Warriors.getLog().severe(" Make sure the skill has proper permissions!");
        } catch (ClassNotFoundException ex) {
            Warriors.getLog().severe(" Class not found loading skill:");
            Warriors.getLog().severe(ex.getMessage());
            Warriors.getLog().severe(" Check 'Skill.yml' main class name!");
        }
        return skill;
    }
    
    public SkillDescriptionFile getSkillDescFile(){
        return skillDesc;
    }
    
    public String getMainClass(){
        return skillDesc.getMainClass();
    }
    
    public String getName(){
        return skillDesc.getName();
    }
}
