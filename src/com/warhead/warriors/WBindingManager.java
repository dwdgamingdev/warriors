/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.warhead.warriors;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 *
 * @author Somners
 */
public class WBindingManager {
    
    private static HashMap<String, HashMap<String, List<String>>> bindMap = new HashMap<String, HashMap<String, List<String>>>();
    
    public WBindingManager(){
    
    }
    
    public boolean bindSkill(String playerName, String itemName, String skillName){
        if(!bindMap.containsKey(playerName)){
            bindMap.put(playerName, new HashMap<String, List<String>>());
        }
        if(!bindMap.get(playerName).containsKey(itemName)){
            bindMap.get(playerName).put(itemName, new ArrayList<String>());
        }
        if(!bindMap.get(playerName).get(itemName).contains(skillName)){
            return false;
        }
        bindMap.get(playerName).get(itemName).add(skillName);
        return true;
    }
    
    public boolean unBindSkill(String playerName, String itemName, String skillName){
        if(bindMap.containsKey(playerName)){
            if(bindMap.get(playerName).containsKey(itemName)){
                if(bindMap.get(playerName).get(itemName).contains(skillName)){
                    bindMap.get(playerName).get(itemName).remove(skillName);
                    return true;
                }
            }
        }
        return false;
    }
    
    public boolean isBound(String playerName, String itemName, String skillName){
        if(bindMap.containsKey(playerName)){
            if(bindMap.get(playerName).containsKey(itemName)){
                    return bindMap.get(playerName).get(itemName).contains(skillName);
            }
        }
        return false;
    }
    
    public String[] getBoundSkillNames(String playerName, String itemName){
        if(bindMap.containsKey(playerName)){
            if(bindMap.get(playerName).containsKey(itemName)){
                    return (String[]) bindMap.get(playerName).get(itemName).toArray();
            }
        }
        return null;
    }
    
    
    
}
