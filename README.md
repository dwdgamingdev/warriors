[![Warriors][Project logo]][Website]

What is Warriors?
-------------------
Warriors is an API built for the Spout project that enables any developer to create a unique RPG feel to their game.

Development Contributions!
----------------
Warriors recieves many generous development contributions, many are in the form of development kits and profilers. A few are listed below!

YourKit is kindly supporting open source projects with its full-featured Java Profiler.
YourKit, LLC is the creator of innovative and intelligent tools for profiling
Java and .NET applications. Take a look at YourKit's leading software products:
[YourKit Java Profiler](http://www.yourkit.com/java/profiler/index.jsp) and
[YourKit .NET Profiler](http://www.yourkit.com/.net/profiler/index.jsp).




[Project Logo]: http://warhead-gaming.com/javadocs/Warriors/Warriors.png
[Website]: http://www.warhead-gaming.com/
